We would like to credit and acknowledge the following:

- [Eleventy](https://www.11ty.dev/) for their Static Site Generator and its [documentation](https://www.11ty.dev/docs/).
- [The Net Ninja](https://www.youtube.com/channel/UCW5YeuERMmlnqo4oq8vwUpg) for their awesome CSS Grid [tutorial series](https://youtube.com/playlist?list=PL4cUxeGkcC9hH1tAjyUPZPjbj-7s200a4).
- [Piccalil](https://piccalil.li/) for the handy eleventy starting guide.
- [Remix Icon](https://remixicon.com/) for the vector icons used throughout the website.