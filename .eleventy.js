const CleanCSS = require("clean-css");
const markdownIt = require("markdown-it");
const markdownItAnchor = require("markdown-it-anchor");
const eleventyPluginTOC = require("@thedigitalman/eleventy-plugin-toc-a11y");

module.exports = function (eleventyConfig) {
  // Passthrough required files to the destination folder (_site/)
  // eleventyConfig.addPassthrougCopy("src/<folder")

  eleventyConfig.addPassthroughCopy("src/assets");

  // Returns a collection of blog posts in reverse date order
  eleventyConfig.addCollection("blog", (collection) => {
    return collection.getFilteredByGlob("./src/blogs/*.md").reverse();
  });

  // Returns a collection of review posts in reverse date order
  eleventyConfig.addCollection("review", (collection) => {
    return collection.getFilteredByGlob("./src/reviews/*.md").reverse();
  });
  
  eleventyConfig.addCollection("recommended", (collection) => {
    return collection.getFilteredByTag('recommended');
  });

  eleventyConfig.addFilter("cssmin", function (code) {
    return new CleanCSS({}).minify(code).styles;
  });

    eleventyConfig.setLibrary("md", markdownIt().use(markdownItAnchor));


    eleventyConfig.addPlugin(eleventyPluginTOC, {
      tags: ["h2", "h3", "h4", "h5", "h6"],
      wrapper: "nav",
      wrapperClass: "nav_toc",
      heading: true,
      headingClass: "nav_toc_heading",
      headingLevel: "h2",
      headingText: "Table of contents",
      listType: "ol",
      listClass: "nav_toc_list",
      listItemClass: "nav_toc_list_item",
      listItemAnchorClass: "nav_toc_list_item_anchor",
    });


  return {
    passthroughFileCopy: true,

    // Tell Eleventy what to process md, data and html templates with
    // Allows use of html files instead of njk files with no issues
    markdownTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    htmlTemplateEngine: "njk",

    // Override/confirm defaults for file paths
    // Paths used in template files are relative to "input"
    dir: {
      input: "src",
      output: "_site",
    },
  };
};
