# Official website for FOSSverse

## Description
FOSSverse is a community-driven platform for discussing and sharing free software. We collate lists of cool and interesting software, contribute to FOSS projects, and communicate over multiple platforms.

This websites purpose is to host various types of content written by or created by members of the FOSSverse community, or to showcase projects that the community would like to support. It will also contain the FOSSverse community event schedule.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## [Authors and acknowledgment](ACKNOWLEDGEMENTS.md)
We do our best to credit everything that helped us create this. If we missed anything make sure to let us know so that we can update the page.

## License

Eleventy components are licensed under [MIT license](https://github.com/11ty/eleventy/blob/master/LICENSE).  
Many icons are taken from [Remix Icon](https://remixicon.com/) and are originally licensed with [Apache License version 2.0](https://github.com/Remix-Design/remixicon/blob/master/License).
Everything else (unless stated otherwise):  
<br>
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
