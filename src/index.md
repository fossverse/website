---
title: 'Welcome to FOSSverse'
layout: 'layouts/homepage.html'
metaDesc: 'FOSSverse, the Free and Open Source community.'
---

FOSSverse is a community-driven platform for discussing and sharing free software. We collate lists of cool and interesting software, contribute to FOSS projects, and communicate over multiple platforms.

At FOSSverse we want to encourage:

- Contributing to FOSS projects
- Sharing FOSS projects that you want to support
- Trying out FOSS alternatives to proprietary software