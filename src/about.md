---
title: 'About us'
layout: 'layouts/about.html'
permalink: '/aboutus/index.html'
---

# How FOSSverse Started

FOSSverse started as a side project when we were in university under the name "Tech Vision", originally meant as a general technology focused server as opposed to a FOSS-focused server. As time went on, the server shifted focus as our interests did. While exploring Linux and Linux related topics, we came across the idea of free and open source software, and shifted the focus of the server accordingly.

The idea back then was to have a social space for people interested in technology in general. Now it is a social space for people interested in Linux and FOSS.

# What is this website for?
The purpose of the website will be to host any writing or work created by us or the people in the community. This will include our blogs, blogs our community wants to write for the website, various reviews of FOSS software, lists of software, or software projects created by our community members.

You will find all of this and more. Just whatever we want to post that is related to FOSS, really.