---
title: 'FOSSverse reviews'
layout: 'layouts/posts.html'
pagination:
  data: collections.review
  size: 6
permalink: 'reviewlist{% if pagination.pageNumber > 0 %}/page/{{ pagination.pageNumber }}{% endif %}/index.html'
paginationPrevText: 'Newer posts'
paginationNextText: 'Older posts'
paginationAnchor: '#post-list'
postType: 'review'
---
Reviews by the FOSSverse's finest ^^