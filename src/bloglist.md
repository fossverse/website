---
title: 'FOSSverse blogs'
layout: 'layouts/posts.html'
pagination:
  data: collections.blog
  size: 6
permalink: 'bloglist{% if pagination.pageNumber > 0 %}/page/{{ pagination.pageNumber }}{% endif %}/index.html'
paginationPrevText: 'Newer posts'
paginationNextText: 'Older posts'
paginationAnchor: '#post-list'
postType: 'blog'
---